import { HelperModule } from './helpers';
import { Product } from '../../app/products/product/product';
import { ProductModule } from '../../assets/js/product';

const helpers = new HelperModule.HelpersMethods();
const productMethods = new ProductModule.ProductMethods();

export module CartModule {
    export interface CartMethods{
            addProductToCart(s: string): string;
    }
  export class CartMethods {
            add(input: HTMLInputElement, product: Product, opakValue: number): void{
                //get input value
                let value = parseFloat(input.value);
                /*
                    -first => input is empty so the value will be opak value or 1 if product doesnt have round to table
                    -second => case when user clicks on add button, to the existing input value add opak value for each click
                    -finally => remove data-touched attribute for detecting the second case
                */
                if(opakValue !== null){
                    //first
                    if(input.value == ''){
                        value = opakValue;
                        input.value = value.toString();
                    }
                    //second
                    else if(input.getAttribute('data-touched') != 'true'){
                        const rounded_value = Math.round((parseFloat(input.value) + opakValue) * 100) / 100;
                        input.value = rounded_value.toString();
                        value = parseFloat(input.value);
                    }
                    //finally
                    else
                        input.removeAttribute('data-touched');
                }
                //product id
                const product_id = product.productID;
                if(productMethods.checkProductAvailability(product, value)){
                    //call cart local storage
                    this.cartLocalStorage(value, product);
                }
                else
                    return;
            }
            remove(id: number): void{
                //create empty cartItems array
                let cartItems: Product[] = [];
                //get cart items from local storage
                let cartLs = JSON.parse(localStorage.getItem('cart'));
                //add all products to cartItems array
                for(let i = 0; i < cartLs.length; i++)
                    cartItems.push(cartLs[i]);
                //initialize product_index and check if product with this ID exists
                let product_index;
                product_index = cartItems.findIndex(x => x.productID == id);
                //if yes then remove it and update localStorage cart
                if(product_index > -1){
                    cartItems.splice(product_index, 1);
                    localStorage.setItem('cart', JSON.stringify(cartItems));
                }
            }
            update(product: Product, input: HTMLInputElement, opakValue: number): void{
                const product_id = product.productID;
                const td_add_round_actions = <HTMLElement>(document.querySelector('tr[data-product-row="' + product_id + '"] td.add-round-actions'));
                let table_round = td_add_round_actions.getElementsByClassName('round-table');
                if(table_round.length > 0){
                //get rounded value
                const round_val = parseFloat(table_round[0].getElementsByClassName('round-value')[0].innerHTML);
                //call the round method
                const rounded_valued = productMethods.roundTo(product_id, parseInt(input.value), round_val, true);
                this.add(input, product, opakValue);
                }
                else
                    this.add(input, product, opakValue);
            }
            updatePrices(product: Product, value: number): void{
                //initial discount
                productMethods.initialDiscount(product, value);
                //set product price netto
                product.priceNetto = parseFloat((<HTMLElement>document.querySelector('tr[data-product-row="' + product.productID + '"] .product-price-netto')).innerHTML.split(' ')[0]);
                //set product price brutto
                product.priceBrutto = parseFloat((<HTMLElement>document.querySelector('tr[data-product-row="' + product.productID + '"] .product-price-netto')).innerHTML.split(' ')[0]);
                //set discount init
                product.discountInit = value;
                //update cart products calling cart local storage
                this.cartLocalStorage(product.quantity, product);
                //show status message
                //helpers.createStatusMessage('warning', `Zaktualizowano ceny dla produktu ${product.name}`);
            }
            cartLocalStorage(value: number, product: Product): void{
                const id = product.productID;
                let cart: Product[];
                let arr: Product[];
                //check if local storage cart exists, if not then create it and set it to an ampty array
                if(localStorage.getItem('cart') === null)
                    cart = [];
                //if cart exists then get it
                else
                    cart = JSON.parse(localStorage.getItem('cart'));
                //check if product is already in this cart
                let product_index = cart.findIndex(x => x.productID == id);
                //set product quantity
                product.quantity = value;
                //if no then add a new product
                if(product_index == -1 && product !== undefined){
                    cart.push(
                        product
                    )
                }
                //if yes then set new value
                else{
                    cart[product_index] = product
                }
                //finally set cart local storage
                localStorage.setItem('cart', JSON.stringify(cart));
            }
            setHTMLCart(value: number, product: Product): void{
                const id = product.productID;
                const li = helpers.createHTMLElement('li', 'cart-item',`Product ${product.name} (${value})`);
                //set li id for each product
                li.setAttribute('data-id', id.toString());
                //select and convert html element
                let found_li = (<HTMLUListElement>document.querySelector('#cart-items li[data-id="' + id + '"]'));
                //check if li with this id exists
                if(found_li != null){
                    //change product quantity
                    if(found_li)
                        found_li.innerHTML = `Product ${product.name} (${value})`;
                }
            }
        }
}