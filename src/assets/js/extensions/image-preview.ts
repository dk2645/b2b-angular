import { HelperModule } from './../helpers';

const helpers = new HelperModule.HelpersMethods();

export module ImagePreviewModule {
    export interface ImagePreviewModule{
            addProductToCart(s: string): string;
    }
    export class ImagePreview {
        imagePreview(element: HTMLImageElement, navigation: boolean, download: boolean): void{
            const active_image_preview = document.body.getElementsByClassName('image-preview-wrapper').length == 1 ? true : false;
            //set variables
            let current_row, image_preview_wrapper, image_holder, image, image_src, image_width, image_height, data_row, navigate_prev, navigate_next, image_name;
            //current row
            current_row = element.parentNode.parentNode;
            //get image width and height
            image_width = element.naturalWidth;
            image_height = element.naturalHeight;
            if(!active_image_preview){
                //create image preview wrapper
                image_preview_wrapper = helpers.createHTMLElement('div', 'image-preview-wrapper');
                //create image holder
                image_holder = helpers.createHTMLElement('div', 'image-holder');
                //create image
                image = helpers.createHTMLElement('img', 'image-preview');
                //get image src attribute
                image_src = element.getAttribute('src');
                //set image styles
                image.setAttribute('style', `max-width: 1000px; max-height: 800px;`);
                //set iamge src
                image.setAttribute('src', image_src);
                //image name
                image_name = helpers.createHTMLElement('p', 'image-name', current_row.querySelector('td.product-name').innerText);
                //navigation --start
                if(navigation){
                    //create prev and next navigation arrows
                    navigate_prev = helpers.createHTMLElement('i', 'material-icons navigate-prev', 'navigate_before');
                    navigate_next = helpers.createHTMLElement('i', 'material-icons navigate-next', 'navigate_next');
                    //append prev click
                    navigate_prev.onclick = function(){
                        this.navigation(navigate_prev, current_row, false, navigation, download);
                    }.bind(this);
                    //append next click
                    navigate_next.onclick = function(){
                        this.navigation(navigate_next, current_row, true, navigation, download);
                    }.bind(this);
                    //call firstLastImageNavigation
                    //this.firstLastImageNavigation(current_row, navigate_prev);
                    this.firstLastImageNavigation(current_row, navigate_next);
                    //add navigation arrows to the image holde
                    image_holder.appendChild(navigate_prev);
                    image_holder.appendChild(navigate_next); 
                }
                //navigation --end
                if(current_row.previousSibling.nodeName.toLowerCase() != 'tr')
                     navigate_prev.style.display = 'none';
                if(current_row.nextSibling.nodeName.toLowerCase() != 'tr')
                    navigate_next.style.display = 'none';
                //download --start
                let download_link, download_icon;
                if(download){
                    //create download link
                    download_link = document.createElement('a');
                    //create download icon
                    download_icon = helpers.createHTMLElement('i', 'material-icons download', 'file_download');
                    //add icon to the link
                    download_link.appendChild(download_icon);
                    //cal the download
                    this.download(download_link, image_src);
                    //add download link to the image holder
                    image_holder.appendChild(download_link);
                }
                else
                    download_link = null;
                //download --end
    
                //close --start
                //create close icon
                const close_icon = helpers.createHTMLElement('i', 'material-icons close', 'close');
                //call close
                this.close(close_icon);
                //call looseFocus
                this.looseFocus(image_preview_wrapper);
                //add close icon to the image holder
                image_holder.appendChild(close_icon);
                //close --end
    
                //active keys --start
                this.activeKeys(download_link, navigate_prev, navigate_next);
                //active keys --end
    
                //set image holder attributes
                image_holder.setAttribute('style', `width: ${image_width}px; max-width: 1000px; max-height: 800px;`);
                //add image to the image holder     
                image_holder.appendChild(image);
                //add image name to the image holder
                image_holder.appendChild(image_name);
                //add image holder to the preview wrapper
                image_preview_wrapper.appendChild(image_holder);
                //finally image preview is ready add it to the document
                document.body.appendChild(image_preview_wrapper);    
            }
            else{
                //get image
                image = document.querySelector('.image-preview');
                //get image name
                image_name = document.querySelector('.image-name');
                //change the image name
                image_name.innerText = current_row.querySelector('td.product-name').innerText;
                image_src = element.getAttribute('src');
                //set image src
                image.setAttribute('src', image_src);
                //get image holder
                image_holder = document.querySelector('.image-holder');
                //set image holder styles
                image_holder.setAttribute('style', `width: ${image_width}px; max-width: 1000px; max-height: 800px;`);
    
                //download --start
                if(download)
                    this.download((<HTMLAnchorElement>document.querySelector('.download').parentNode), element.getAttribute('src'))
                //download --end
    
                //navigation --start
                //get navigation arrows
                navigate_prev = document.querySelector('.navigate-prev');
                navigate_next = document.querySelector('.navigate-next');
                if(current_row.previousSibling.nodeName.toLowerCase() == 'tr')
                    navigate_prev.style.display = 'block';
                else
                    navigate_prev.style.display = 'none';
               if(current_row.nextSibling.nodeName.toLowerCase() == 'tr')
                   navigate_next.style.display = 'block';
               else
                    navigate_next.style.display = 'none';
                //call firstLastImageNavigation
                this.firstLastImageNavigation(current_row, navigate_prev);
                this.firstLastImageNavigation(current_row, navigate_next);
                navigate_prev.onclick = function(){
                    this.navigation(navigate_prev, current_row, false, navigation, download);
                }.bind(this);
                navigate_next.onclick = function(){
                    this.navigation(navigate_next, current_row, true, navigation, download);
                }.bind(this);
                //navigation --end
            }
        }
        navigation(navigate: HTMLElement, current_row: any, move_next: boolean, navigation: boolean, download: boolean): void{
                let row;
                if(move_next)
                    row = <HTMLElement>(current_row.nextSibling);
                else
                    row = <HTMLElement>(current_row.previousSibling);
                if(!helpers.isNull(row)){
                    let image = <HTMLImageElement>(row.children[0].childNodes[0]);
                    if(!helpers.isNull(image))
                        this.imagePreview(image, navigation, download);
                }
                else{
                    navigate.remove();
                    return;
                }
        }
        looseFocus(image_preview_wrapper: HTMLElement): void{
            /* looseFocus
                --check for loose focus, check where user clicked and 
                if it's not navigation arrows, close button or download button then close the preview
            */
            image_preview_wrapper.addEventListener('click', function(e){
                const event_target_classes = (<Element>e.target).classList;
                if(!event_target_classes.contains('navigate-prev') && !event_target_classes.contains('navigate-next') 
                    && !event_target_classes.contains('download') && !event_target_classes.contains('close')){
                    helpers.removeHTMLElement('.image-preview-wrapper');
                }
            })
        }
        activeKeys(download_link?: HTMLElement, navigate_prev?: HTMLElement, navigate_next?: HTMLElement){
            /* active keys
                (enter) click to download image
                (close) click to close the preview
                (prev_arrow) click to see the previous image
                (next_arrow) click to see next image
            */
            document.onkeydown = function(e) {
                switch (e.keyCode) {
                    case 13:
                        if(download_link !== null)
                            download_link.click();
                    case 27:
                        this.removeHTMLElement('.image-preview-wrapper');
                    case 37:
                        if(navigate_prev !== null)
                            navigate_prev.click();
                        break;
                    case 39:
                        if(navigate_next !== null)
                            navigate_next.click();
                        break;
                }
            }.bind(this);
        }
        download(download_link: HTMLAnchorElement, image_src: string): void{
            /* download
                --get image src and download image, save as product name
            */
            download_link.onclick = function(){
                download_link.href = image_src;
                let download_name = image_src.split('/').splice(-1)[0].replace('.jpg', '').replace('.png', '');
                download_link.download = download_name;
            };
        }
        close(close_icon: HTMLElement): void{
            /* close
                --remove image preview
            */
            close_icon.onclick = function(image_preview_wrapper){
                helpers.removeHTMLElement('.image-preview-wrapper');
            }.bind(this);
        }
        firstLastImageNavigation(current_row: HTMLElement, navigate: HTMLElement): void{
            /* firstLastImageNavigation
                if this is the first or last image disable previous navigate arrow or next navigate arrow
            */
            /*
            if(current_row.previousSibling.nodeName.toLowerCase() != 'tr'){
                navigate.style.display = 'none';
            }
            if(current_row.nextSibling.nodeName.toLowerCase() != 'tr')
                navigate.style.display = 'none';
            */
        }
      }
}