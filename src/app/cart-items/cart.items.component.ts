import { Component, ElementRef, ViewChild, Renderer } from '@angular/core';
import { CartModule } from '../../assets/js/cart';
import { CartService } from '../cart/services/cart.service';
import { Product } from '../products/product/product';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../shared/_services/languages.service';
import { SharedService } from '../shared/_services/shared.service';

@Component({
    selector: 'cart-items',
    templateUrl: 'cart.items.component.html'
})

export class CartItemsComponent{
    @ViewChild('cartItems') private cartItems;
    cartMethods = new CartModule.CartMethods();
    public products: Product[];
    public showCart: boolean = false;
    listenFunc: Function;
    public emptyCartInfo: boolean = true;

    constructor(private cartService: CartService, public elementRef: ElementRef, public renderer: Renderer,
                private translate: TranslateService, private sharedService: SharedService) {
                    let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
                    translate.use(lang);
                }

    cartItemsEdit():void{
        //check if user is logged in
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser != null){
            //create list with cart items after view is already rendered
            let cartItems = JSON.parse(localStorage.getItem('cart'));
            if(cartItems != null){
                cartItems.forEach(p => {            
                    this.cartMethods.setHTMLCart(p.quantity, p);
                });
            }
        }
    }

    seeMoreCart(){
        //add html event focusout on cart items element
        this.listenFunc = this.renderer.listen(this.elementRef.nativeElement, 'focusout', (event) => {
            //get what was clicked
            let clicked_out = event.relatedTarget
            //check if element what was clicked is button see more and if it is then prevent from closing cart items element
            if(event.relatedTarget !== null && clicked_out.classList.contains('cart-check') || event.relatedTarget !== null && clicked_out.classList.contains('cart-product-link')){
                if(clicked_out.classList.contains('cart-check') || clicked_out.classList.contains('cart-product-link'))
                    event.preventDefault();
            }
            else
                this.showCart = false;
        });
    }

    showCartClick(){
        this.products = this.cartService.getCartItems().filter(Boolean);
        //if there is any product in cart then show button if not then show message that the cart is 
        this.emptyCartInfo = this.cartService.getCartItems().filter(Boolean).length >= 1 ? false : true;
        //show/hide cart
        this.showCart = !this.showCart;
        //add focusout on cart items element
        this.seeMoreCart();
    }
}