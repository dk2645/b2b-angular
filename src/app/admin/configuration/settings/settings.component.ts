import { Component, OnInit, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { SettingsService } from '../../_services/settings.service';
import 'rxjs/add/operator/toPromise';
import { DialogMessageComponent } from '../../../shared/_popups/dialog.message.component';
import { PopupsService } from '../../../shared/_services/popups.service';

@Component({
    templateUrl: 'settings.component.html'
})

export class SettingsComponent implements OnInit {
    @ViewChild('toggleAllSettings') private toggleAllSettings;
    public settings: any = [];
    constructor(private http: Http, private settingsService: SettingsService, private dialog: DialogMessageComponent, private popupsService: PopupsService) { 

    }

    ngOnInit() {
        this.loadSettings();
        //this.toggleAll();
     }

    loadSettings(){
        this.settingsService.getSettings().then(v => Promise.resolve(v).then(function(value) {
            Object.keys(value).forEach(function (key){
                this.settings.push(value[key]);
            }.bind(this));
                }.bind(this), function(value) {
        }));
    }
    updateSettings(set: any, value: any){
        //find settings index
        let found_set = this.settings.findIndex(v => v.key === set.key);
        //settings type boolean
        if(typeof(value) === "boolean"){
            //set value
            const checked = value == true ? 'on' : 'off';
            this.settings[found_set].value = checked;
        }
        //settings type html
        else if(typeof(value) == 'string'){
            this.settings[found_set].value = value;
        }        
    }
    saveSettings(){
        //get settings
        this.http.post('/post/', this.settings).subscribe(v => {
            console.log('hero');
        }, err => {
            console.log(this.settings);
            console.log(JSON.stringify(this.settings));
            this.popupsService.dialog('alert', `Error: ${err.statusText} (${err.status}). Prosimy skontaktowac sie z administracja`, 'highlight_off', '');
            this.dialog.openDialog();
        })
    }
    toggleGroup(name: string){
        //get all settings cards by group name
        let cards = document.querySelectorAll(`.single-set.${name}`);
        //toggle
        this.toggle(cards);
    }
    toggleAll(): void{
        //get all settings cards
        let cards = document.querySelectorAll('.single-set');
        //toggle
        this.toggle(cards);
        //change icon
        let current_visibility_all_icon = this.toggleAllSettings.nativeElement.innerHTML;
        this.toggleAllSettings.nativeElement.innerHTML = current_visibility_all_icon == 'visibility' ? 'visibility_off' : 'visibility';
    }
    toggle(items: any): void{
        for(let i = 0; i < items.length; i++){
            //store class list and hide/show depends on button click
            let items_classList = items[i].classList
            if(items_classList.contains('hide'))
                items_classList.remove('hide');
            else
                items_classList.add('hide');
        }
    }
    ngAfterViewInit(){
        this.toggleAll();
    }
}