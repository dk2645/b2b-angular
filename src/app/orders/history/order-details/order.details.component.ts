import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'order.details.component.html'
})

export class OrderDetailsComponent implements OnInit{
    private orderID: number;
    constructor(private router: Router, private route: ActivatedRoute) { }

    ngOnInit(){
        this.route.params.subscribe(
            (params: any) => {
                this.orderID = params['id']
            }
        )
    }

    
}