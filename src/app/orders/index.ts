export * from '../orders/history/orders-history/orders.history.component';
export * from '../orders/history/order-details/order.details.component';
export * from '../orders/order/order.component';
export * from './order/payments-shipping/order.payments-shipping.component';
export * from './order/address/order.address.component';
export * from './order/address/contacts-list/order.contacts-list.component';
export * from './order/confirm/order.confirm.component';
export * from './order/_services/order.service';