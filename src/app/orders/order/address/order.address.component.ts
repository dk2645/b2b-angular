import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {MdDialog, MdDialogRef} from '@angular/material';
import { OrderContactsListComponent } from './contacts-list/order.contacts-list.component';
import { OrderService } from '../_services/order.service';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../../../shared/_services/languages.service';
import { SharedService } from '../../../shared/_services/shared.service';
import { MdSnackBar } from '@angular/material';


@Component({
    selector: 'order-address',
    templateUrl: 'order.address.component.html',
    providers: [ OrderContactsListComponent ]
})

@Injectable()
export class OrderAddressComponent {
    selectedOption: string;
    constructor(public dialog: MdDialog, public orderContactsList: OrderContactsListComponent, private orderService: OrderService,
                private translate: TranslateService, private sharedService: SharedService, public snackBar: MdSnackBar, private router: Router) { 
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        translate.use(lang);
    }

    chooseContact(){
        console.log()
        this.orderContactsList.openDialog();
    }

    finalizeOrder(inputs_wrarapper: HTMLElement): void{
        //console.log(inputs_wrarapper);
        const elements = (inputs_wrarapper.children);
        for (let i = 0; i < elements.length; i++) {
            //get input wrapper
            const input_wrapper = (<HTMLElement>elements[i].children[0].children[0].children[0]);
            //get input value
            const input = (<HTMLInputElement>input_wrapper.children[0]);
            const input_name = input.getAttribute('placeholder');
            if(input.value == ''){
                this.statusInfo(`${input_name} ${this.translate.instant('is_required')}`, '', 'alert');
                return;
            }
            else{
                this.router.navigate(['/customer/order-confirm']);
            }
        }
    }


    statusInfo(message: string, value: string, type: string) {
        this.snackBar.open(message, value, {
          duration: 2000,
          extraClasses: [type]
        });
      }
}