import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../products/product/product';
import { CartService } from '../../cart/services/cart.service';
import { PopupsService } from '../../shared/_services/popups.service';
import { DialogMessageComponent } from '../../shared/_popups/dialog.message.component';
import { OrderContactsListComponent } from './address/contacts-list/order.contacts-list.component';
import { ImagePreviewModule } from '../../../assets/js/_index';
import { OrderAddressComponent } from './address/order.address.component';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../../shared/_services/languages.service';
import { SharedService } from '../../shared/_services/shared.service';
import { MdSnackBar } from '@angular/material';
import { OrderConfirmComponent} from './confirm/order.confirm.component';
import { OrderService } from './_services/order.service';


@Component({
    templateUrl: 'order.component.html',
    providers: [DialogMessageComponent, OrderContactsListComponent, OrderConfirmComponent]
})

export class OrderComponent implements OnInit {
    public products: Product[] = [];
    public settings: any = '';
    constructor(private cartService: CartService, public orderContactsList: OrderContactsListComponent, private popupsService: PopupsService, private dialog: DialogMessageComponent,
                private translate: TranslateService, private sharedService: SharedService, public snackBar: MdSnackBar, private router: Router,
                private orderConfirm: OrderConfirmComponent, private orderService: OrderService) {
        this.setTableProducts();
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        translate.use(lang);
     }
     
     imagePreviewExt = new ImagePreviewModule.ImagePreview();

     ngOnInit(){
        //initial multi languages
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        //initial settings
        this.settings = this.sharedService.globalSettings == null || this.sharedService.globalSettings == undefined 
        ? this.sharedService.getSettings().subscribe(settings => this.settings = settings) : this.sharedService.globalSettings;
        this.settings = this.sharedService.globalSettings;
     }

    setTableProducts(){
        //get products
        this.products = this.cartService.cartProducts.filter(Boolean);
        //check products count in cart if cart is empty then redirect
        if(this.products.length == 0){
            this.popupsService.dialog('natural', 'Koszyk jest pusty.', 'info_outline', '/');
            this.dialog.openDialog();
        }
    }

    imagePreview(element: HTMLImageElement){
        //this.helpers.imagePreview(element, product_name);
        //let allow_product_image_preview =  (this.settings.allow_product_image_preview.value == 'true');
        let show_image_preview_navigation = (this.settings.show_image_preview_navigation.value == 'true');
        let allow_to_image_preview_download = (this.settings.allow_to_image_preview_download.value == 'true');
        this.imagePreviewExt.imagePreview(element, show_image_preview_navigation, allow_to_image_preview_download);
    }

    /*
    finalizeOrder(inputs_wrarapper: HTMLElement): void{
        //console.log(inputs_wrarapper);
        const elements = (inputs_wrarapper.children);
        for (let i = 0; i < elements.length; i++) {
            //get input wrapper
            const input_wrapper = (<HTMLElement>elements[i].children[0].children[0].children[0]);
            //get input value
            const input = (<HTMLInputElement>input_wrapper.children[0]);
            const input_name = input.getAttribute('placeholder');
            if(input.value == ''){
                this.statusInfo(`${input_name} ${this.translate.instant('is_required')}`, '', 'alert');
                return;
            }
            else{
                this.router.navigate(['/customer/order-confirm']);
            }
        }
    }
    */

    finalizeOrder(){
        //code
        this.orderService.setOrder();
        this.router.navigate(['/customer/order-confirm']);
    }

    chooseContact(){
        this.orderContactsList.openDialog();
    }

    statusInfo(message: string, value: string, type: string) {
        this.snackBar.open(message, value, {
          duration: 2000,
          extraClasses: [type]
        });
    }

}