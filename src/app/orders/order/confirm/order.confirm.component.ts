import { Component, OnInit, Injectable } from '@angular/core';
import { OrderService } from '../_services/order.service';
import { CartService } from '../../../cart/services/cart.service';
import { ProductService } from '../../../products/services/product.service';
import { Router } from '@angular/router';

@Component({
    templateUrl: 'order.confirm.component.html'
})


@Injectable()
export class OrderConfirmComponent implements OnInit {
    constructor(private orderService: OrderService, private cartService: CartService, private productService: ProductService, private router: Router) { }

    ngOnInit() { 
        this.checkOrderStatus();
        window.scrollTo(0, 0);
    }

    checkOrderStatus(): void{
        const order_status: Boolean = this.orderService.getOrderStatus();
        if(order_status){
            this.cartService.cartProducts = [];
            localStorage.removeItem('cart');
        }
        else{
            this.router.navigate(['/customer/products-list'])
        }
    }
}