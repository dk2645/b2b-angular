export class Registration{

constructor(
    public email: string,
    public confirmEmail: string,
    public firstName: string,
    public lastName: string,
    public street: string,
    public no: string,
    public no2: string,
    public postCode: string,
    public city: string,
    public cName: string,
    public cAddress: string,
    public cPostCode: string,
    public cCity: string,
    public cTaxId: number,
    public cRegon: number,
    public phone: number,
    public phone2: number,
    public note: string,
    public allowMailing: boolean,
    public conditions: boolean,
    public conditions2: boolean
){}
}