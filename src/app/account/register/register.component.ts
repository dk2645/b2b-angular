import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../../shared/_services/languages.service';
import { SharedService } from '../../shared/_services/shared.service';
import { Registration } from '../_models/registration';

@Component({
    templateUrl: 'register.component.html'
})

export class RegisterComponent {
    public account: Registration | null;
    constructor(private translate: TranslateService, private sharedService: SharedService) {
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        translate.use(lang);
     }
}