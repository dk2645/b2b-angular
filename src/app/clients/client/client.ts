export class Client{
    constructor(
        public clientID: number,
        public email: string,
        public name: string,
        public firstName: string,
        public lastName: string,
        public symbol: string,
        public phone: string,
        public phone2: string,
        public companyName: string,
        public address: string,
        public postCode: string,
        public city: string,
        public country: string,
        public taxID: number,
        public blocked: boolean
    ){}
}