import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Client } from '../client/client';
import { ClientService } from '../services/client.service';

@Component({
    templateUrl: 'change-password-client.component.html',
    providers: [ ClientService ]
})

export class ChangePasswordClientComponent implements OnInit {
    public clientID: number;
    public client: Client;
    constructor(public clientService: ClientService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params.subscribe(
            (params: any) => {
                this.clientID = params['id']
            }
        )
        this.client = this.clientService.getClientById(this.clientID);
     }
}