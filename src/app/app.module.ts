//angular imports --start
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { CdkTableModule } from '@angular/cdk';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
//angular imports --end
import { AuthGuard } from './shared/_guards/auth.guard'
//shared services
import { AuthService, UserService, PopupsService } from './shared/_services/index';
//account components
import { RegisterComponent, LoginComponent, PasswordReminderComponent } from './account/index';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
//product components with service
import { ProductDetailsComponent, ProductsListCompontent, ProductService } from './products/index';
//clients component
import { ClientsListComponent } from './clients/clients-list/clients-list.component';
import { ModifyClientComponent } from './clients/modify-client/modify-client.component';
import { ChangePasswordClientComponent } from './clients/change-password-client/change-password-client.component';
import { CartItemsComponent } from './cart-items/cart.items.component';
//cart components with service
import { CartComponent, CartService } from './cart/index';
import { DialogMessageComponent } from './shared/_popups/dialog.message.component';
import { DialogConfirmComponent } from './shared/_popups/confirm/dialog.confirm.component';
//shared material design module
import { MaterialDesignModule } from './shared/_material.design/material.design.module';
import { FillColorComponent } from './shared/_fill.color/fill.color.component';
import { PaymentsComponent } from './payments/payments.component';
import { ComplaintsComponent } from './complaints/complaints.component';
//order componenets
import { OrderService, OrdersHistoryComponent, OrderDetailsComponent, OrderComponent, OrderAddressComponent, OrderPaymentsShippingComponent, OrderContactsListComponent, OrderConfirmComponent } from './orders/index';
import { DocumentsComponent } from './documents/documents.component';
//offers components
import { OffersListComponent, OfferDetailsComponent } from './offers/index';
//news components with service
import { NewsComponent } from './news/news.compontent';
import { NewsService } from './news/_services/news.service';
import { CheckListComponent } from './check-list/check-list.component';
import { SharedModule } from './shared/_modules/shared.module';
import { LanguagesService } from './shared/_services/languages.service';
import { ClientSidebarComponent} from './layout/sidebar/client-sidebar.component';
import { AdminModule } from './admin/admin.module';
import { SharedService } from './shared/_services/shared.service';



export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, "./assets/data/languages/", ".json");
}

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    MaterialDesignModule,
    CdkTableModule,
    SharedModule,
    AdminModule,
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [Http]
        }
    })
  ],
  declarations: [
    AppComponent,
    //account
    LoginComponent,
    RegisterComponent,
    PasswordReminderComponent,
    //products
    ProductsListCompontent,
    ProductDetailsComponent,
    //cart
    CartComponent,
    CartItemsComponent,
    //orders
    OrdersHistoryComponent,
    OrderDetailsComponent,
    OrderComponent,
    OrderAddressComponent,
    OrderPaymentsShippingComponent,
    OrderContactsListComponent,
    OrderConfirmComponent,
    //offers
    OffersListComponent,
    OfferDetailsComponent,
    CheckListComponent,
    //clients
    ClientSidebarComponent,
    ClientsListComponent,
    ModifyClientComponent,
    ChangePasswordClientComponent,
    //news
    NewsComponent,
    //others
    HomeComponent,
    DashboardComponent,
    FillColorComponent,
    PaymentsComponent,
    ComplaintsComponent,
    DocumentsComponent,
    DialogMessageComponent,
    DialogConfirmComponent,
  ],
  providers: [
    AuthGuard,
    AuthService,
    UserService,
    ProductService,
    PopupsService,
    CartService,
    LanguagesService,
    SharedService,
    OrderService,
    NewsService,
    HttpClient
  ],
  entryComponents: [DialogMessageComponent, OrderContactsListComponent, DialogConfirmComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }

// export class TranslateHttpLoader implements TranslateLoader {
//     constructor(private http: HttpClient, private prefix: string = "/assets/i18n/", private suffix: string = ".json") {}

//     /**
//      * Gets the translations from the server
//      * @param lang
//      * @returns {any}
//      */
//     public getTranslation(lang: string): any {
//         return this.http.get(`${this.prefix}${lang}${this.suffix}`);
//     }
// }