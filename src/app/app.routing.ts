//angular imports
import { Routes, RouterModule } from '@angular/router';
//account components
import { RegisterComponent, LoginComponent, PasswordReminderComponent } from './account/index';
import { DashboardComponent } from './dashboard/dashboard.component';
//product components
import { ProductsListCompontent, ProductDetailsComponent } from './products/index';
//clients components
import { ClientsListComponent } from './clients/clients-list/clients-list.component';
import { ModifyClientComponent } from './clients/modify-client/modify-client.component';
import { ChangePasswordClientComponent } from './clients/change-password-client/change-password-client.component';
import { CartComponent } from './cart/cart.component';
import { AuthGuard } from './shared/_guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { PaymentsComponent } from './payments/payments.component';
import { ComplaintsComponent } from './complaints/complaints.component';
//order components
import { OrderDetailsComponent, OrdersHistoryComponent, OrderComponent, OrderConfirmComponent } from './orders/index';
import { DocumentsComponent } from './documents/documents.component';
//offer components
import { OffersListComponent, OfferDetailsComponent } from './offers/index';
import { CheckListComponent } from './check-list/check-list.component';
import { ClientSidebarComponent} from './layout/sidebar/client-sidebar.component';
//news components
import { NewsComponent } from './news/news.compontent';
//admin
import { AdminDashboardComponent } from './admin/dashboard/admin.dashboard.component';
//admin configurations
import { SettingsComponent, AdminGeneralConfigurationComponent, AdminContactConfigurationComponent,
         AdminStocksConfigurationComponent, AdminClientsConfigurationComponent, AdminPaymentsConfigurationComponent,
         AdminShippingConfigurationComponent, AdminStatusConfigurationComponent, AdminNotificationsConfigurationComponent,
         AdminEmailConfigurationComponent, AdminNewsletterConfigurationComponent, AdminPriceListsConfigurationComponent,
         EventLogComponent } from './admin/configuration/index';
//admin tools
import { AdminComplaintsComponent } from './admin/tools/complaints/admin.complaints.component';
//admin sale
import { AdminClientsSaleComponent, AdminOrdersSaleComponent, AdminPromotionCodesSaleComponent, AdminStocksSaleComponent,
         AdminTradersSaleComponent, AdminLoyaltySaleComponent, AdminProductsSaleComponent } from './admin/sale/index';
//admin content-management
import { AdminMenuContentManagementComponent, AdminNewsContentManagementComponent, AdminStaticPagesContentManagementComponent  } from './admin/content-management/index';


const appRoutes: Routes = [
    //account
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'password-remind', component: PasswordReminderComponent },
    { path: 'check-list', component: CheckListComponent },
    { path: 'home', component: DashboardComponent},
    { path: '', component: HomeComponent},
    //customer routes
    { path: 'customer', children:[
        { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
        //product routes
        { path: 'products-list', component: ProductsListCompontent},
        { path: 'product/:id', component: ProductDetailsComponent },
        //clients routes
        { path: 'clients-list', component: ClientsListComponent},
        { path: 'modify-client/:id', component: ModifyClientComponent},
        { path: 'change-password-client/:id', component: ChangePasswordClientComponent },
        { path: 'cart', component: CartComponent },
        { path: 'payments', component: PaymentsComponent},
        { path: 'complaints', component: ComplaintsComponent },
        //order routes
        { path: 'orders-history', component: OrdersHistoryComponent },
        { path: 'order-details/:id', component: OrderDetailsComponent },
        { path: 'order-confirm', component: OrderConfirmComponent },
        { path: 'order', component: OrderComponent },
        { path: 'documents', component: DocumentsComponent },
        //offer routes
        { path: 'offers-list', component: OffersListComponent },
        { path: 'offer-details/:id', component: OfferDetailsComponent },
        //news routes
        { path: 'news', component: NewsComponent },
        { path: '' , component: ClientSidebarComponent, outlet: 'sidebar'},
        //otherwise redirect to home
        { path: '**', redirectTo: '' }  
  ]},
  //admin routes
  { path: 'admin', children:[
     { path: 'dashboard', component: AdminDashboardComponent},
      //admin conigurations
     { path: 'configuration', children: [
        { path: 'settings', component: SettingsComponent},
        { path: 'general', component: AdminGeneralConfigurationComponent},
        { path: 'contact', component: AdminContactConfigurationComponent},
        { path: 'price-lists', component: AdminPriceListsConfigurationComponent },
        { path: 'stocks', component: AdminStocksConfigurationComponent},
        { path: 'clients', component: AdminClientsConfigurationComponent},
        { path: 'payments', component: AdminPaymentsConfigurationComponent },
        { path: 'shipping', component: AdminShippingConfigurationComponent },
        { path: 'status', component: AdminStatusConfigurationComponent },
        { path: 'notifications', component: AdminNotificationsConfigurationComponent},
        { path: 'email', component: AdminEmailConfigurationComponent},
        { path: 'newsletter', component: AdminNewsletterConfigurationComponent },
        { path: 'eventlog', component: EventLogComponent }
     ]},
     //admin tools
     { path: 'tools', children: [
        { path: 'complaints', component: AdminComplaintsComponent },
     ]},
     //admin sale
     { path: 'sale', children: [
         { path: 'clients', component: AdminClientsSaleComponent },
         { path: 'orders', component: AdminOrdersSaleComponent },
         { path: 'promotion-codes', component: AdminPromotionCodesSaleComponent },
         { path: 'stocks', component: AdminStocksSaleComponent },
         { path: 'traders', component: AdminTradersSaleComponent },
         { path: 'loyalty', component: AdminLoyaltySaleComponent },
         { path: 'products', component: AdminProductsSaleComponent }
     ]},
     { path: 'content-management', children: [
         { path: 'news', component: AdminNewsContentManagementComponent },
         { path: 'static-pages', component: AdminStaticPagesContentManagementComponent },
         { path: 'menu', component: AdminMenuContentManagementComponent }
     ]}
  ]}
];

export const routing = RouterModule.forRoot(appRoutes);

// export const routing = RouterModule.forRoot([
//     admin_routes
// ]);