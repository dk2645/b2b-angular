import { Component } from '@angular/core';

@Component({
    templateUrl: 'check-list.component.html',
    styleUrls: ['check-list.component.css']
})

export class CheckListComponent {
    public checkList = [
        { id: 1, content: 'Dodawanie do koszyka z poziomu listy jak i widoku pojedynczego produktu)', state: 'not-working' },
        { id: 2, content: 'Zapobieganie wpisywania w ilosci liczby wiekszej od dostepnej/znaku innego niz liczba', state: 'not-working' },
        { id: 3, content: 'Przyznawanie rabatow na poziomie listy produktow/koszyka', state: 'not-working' },
        { id: 4, content: 'Zmienianie ilosci produktu w koszyku', state: 'not-working' },
        { id: 5, content: 'Sprawdzanie dopelniania', state: 'not-working' },
        { id: 6, content: 'Usuwanie pojedynczych produktow z koszyka', state: 'not-working' },
        { id: 7, content: 'Usuwanie wszystkich produktow z koszyka', state: 'not-working' },
        { id: 8, content: 'Informacji o koszyku, tj. poprawne wyswietlanie komunikatow "koszyk jest pusty", "zobacz koszyk"', state: 'not-working' },
        { id: 9, content: 'Stan koszyka podczas wylogowania (czy nadal sa poprawne ilosci', state: 'not-working' },
        { id: 10, content: 'Stanu zapisu koszyka, tj. czy po zamknieciu strony i otworzeniu jej ponownie produkty nadal znajduja sie w koszyku', state: 'not-working' },
        { id: 11, content: 'Stan zalogowania uzytkownika, tj. czy po zamknieciu strony i otworzeniu jej ponownie nadal jest sie zalogowanym ', state: 'not-working' },
        { id: 12, content: 'Czy dzisiaj jest PIATEK?!!', state: 'working' },
    ]
    constructor() {
        console.log(this.checkList);
        this.checkSaveTasksLocalStorage();
     }
     checkSaveTasksLocalStorage(){
         const saved_check_list = JSON.parse(localStorage.getItem('checkList'));
        if(saved_check_list !== null)
            this.checkList = saved_check_list;
     }
     working(id){
        this.checkList.find(x => x.id == id).state = 'working';
        this.saveTasks();
     }
     notWorking(id){
        this.checkList.find(x => x.id == id).state = 'not-working';
        this.saveTasks();
     }
     saveTasks(){
         localStorage.setItem('checkList', JSON.stringify(this.checkList));
     }
     copyList(): void{
         const list_content = JSON.parse(localStorage.getItem('checkList'));
         let texts = [];
         for(let task of list_content){
            let text = task.content + ' --' + task.state + ' ' + document.write("\n");
            texts.push(text);
         }

     }
}