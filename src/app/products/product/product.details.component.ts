import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from './product';
import { ProductService } from '../services/product.service';
import { CartService } from '../../cart/services/cart.service';
import { ProductModule, CartModule, HelperModule } from '../../../assets/js/_index';
import { MdSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../shared/_services/shared.service';

@Component({
    templateUrl: 'product.details.component.html'
})

export class ProductDetailsComponent implements OnInit {
    @ViewChild('roundTable') private roundTable;
    private productID: number;
    public product: Product;
    public cartProduct: Product;
    //settings
    public settings: any = '';
    constructor(private router: Router, private route: ActivatedRoute, private productService: ProductService, 
                private cartService: CartService, private cdr: ChangeDetectorRef, public snackBar: MdSnackBar,
                private translate: TranslateService, private sharedService: SharedService) { 
    }
    productMethods = new ProductModule.ProductMethods();
    cartMethods = new CartModule.CartMethods();
    helpers = new HelperModule.HelpersMethods();

    ngOnInit(){
        this.checkCartProduct();
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        this.settings = this.sharedService.globalSettings == null || undefined ? this.sharedService.getSettings() : this.sharedService.globalSettings;
    }

    addProductToCart(productQty: HTMLInputElement, product: Product, roundValue: HTMLElement): void{
        //get product quantity as number
        const product_qty = parseFloat(productQty.value);
        //check product availability
        if(product_qty <= product.availability || isNaN(product_qty)){
            const opak_value = parseFloat(roundValue.innerHTML);
            this.cartMethods.add(productQty, product, opak_value);
            this.cartService.addProduct(product);
            //show status info
            this.statusInfo(`${this.translate.instant('add_to_cart_message')}: ${product.name}`, `(${product_qty.toString()})`, 'success');
        }
        else{
            this.statusInfo(`${this.translate.instant('availability_message')}`, '', 'alert');
        }
    }

    touched(productQty: HTMLElement){
        this.productMethods.setDataAttribute(productQty, 'touched', 'true');
    }


    checkCartProduct(): void{
        this.route.params.subscribe(
            (params: any) => {
                this.productID = params['id']
            }
        )
        this.product = this.productService.getProductById(this.productID);
        //check if product is in cart if yes then fill the input value
        const found_product: Product = this.cartService.getCartProduct(this.productID);
        if(found_product != null){
            this.cartProduct = found_product;
            //set product quantity
            this.product.quantity = found_product.quantity;
        }
        else{
            this.cartProduct = null;
            this.product.quantity = null;
        }
    }

    roundTo(value: number): void{
        const round_table = this.roundTable.nativeElement;
        if(round_table !== null && round_table !== undefined){
            const rounded_value = parseInt(round_table.children[0].children[0].children[1].innerHTML);
            this.productMethods.roundTo(this.productID, value, rounded_value, false);
            //show status info
             this.statusInfo('Dopelniono do minimum logistycznego', `(${rounded_value})`, 'warning');
        }
    }

    statusInfo(message: string, value: string, type: string) {
        this.snackBar.open(message, value, {
        duration: 2000,
        extraClasses: [type]
        });
    }

    ngAfterViewInit(){
        if(this.cartProduct !== null){
            let set_product_availability = this.productMethods.checkProductAvailability(this.product, this.product.quantity);
        }
        else{
            this.product.availability = this.productService.getProductById(this.productID).availability;
        }
        this.cdr.detectChanges();
    }

}