import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { PopupsService } from '../../shared/_services/popups.service';
import { SettingsService } from '../../admin/_services/settings.service';
import { Key, Settings } from '../../shared/_models/settings';
import { Message } from '../../shared/_models/message';
import { Status } from '../../shared/_models/status';
import { ProductModule, CartModule, HelperModule, ImagePreviewModule } from '../../../assets/js/_index';
import { Product } from '../product/product';
import { ProductService } from '../services/product.service';
import { CartService } from '../../cart/services/cart.service';
//import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../../shared/_services/languages.service';
import { SharedService } from '../../shared/_services/shared.service';
import {Observable} from 'rxjs/Rx';
import { PaginationService } from '../services/pagination.service';
import { SearchFilterPipe } from '../_pipes/search-filter.pipe';
import 'rxjs/add/observable/of';
import * as hero_settings from '../../../assets/js/settings';
import { MdSnackBar } from '@angular/material';

@Component({
    templateUrl: 'products-list.component.html',
    providers: [PaginationService]
})
export class ProductsListCompontent implements OnInit {
  constructor(private productService: ProductService, private cartService: CartService, 
              private paginationService: PaginationService, private settingsService: SettingsService, 
              public snackBar: MdSnackBar, private translate: TranslateService, private sharedService: SharedService,
              private cdr: ChangeDetectorRef){
  }
  //products
  public products: Product[];
  //cart button
  public cartButton: boolean = false;
  //settings
  public settings: any = '';
  //pagination
  private allItems: any[];
  //pager
  pager: any = {};
  //page items
  pagedItems: any[];
  //search filter
  public searchString: string;
  //sort
  public sortType: string = '';
  //custom methods
  exampleDatabase = new ProductService();
  productMethods = new ProductModule.ProductMethods();
  cartMethods = new CartModule.CartMethods();
  helpers = new HelperModule.HelpersMethods();
  imagePreviewExt = new ImagePreviewModule.ImagePreview();

  ngOnInit() {
    //initial multi languages
    let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
    this.translate.use(lang);
    //initial settings
    this.settings = this.sharedService.globalSettings == null || this.sharedService.globalSettings == undefined 
    ? this.sharedService.getSettings().subscribe(settings => this.settings = settings) : this.sharedService.globalSettings;
      this.settings = this.sharedService.globalSettings;
    //initial products
    this.products = this.productService.getProducts();
    // initialize products table and set page 1
    this.allItems = this.products;
    this.setPage(1);
    //check for cart button
    this.checkCartButton();
}

touched(productQty: HTMLInputElement){
  this.productMethods.setDataAttribute(productQty, 'touched', 'true');
}

filterTable(value: string) {
  const filtered_products = this.productService.filterProducts(value.toLowerCase());
  this.products = filtered_products;
  this.allItems = filtered_products;
  this.setPage(1);
}

sort(sortColumn: string): void{
  this.sortType = sortColumn;
  this.products = this.productService.sortProducts(this.products, sortColumn);
}

searchListener(value: string, keyCode: number): void{
  if(keyCode == 13)
    this.filterTable(value);
  else if(value == '')
    this.filterTable(value);
  else
    return;
}

  addProductToCart(product: Product, productRow: HTMLTableRowElement, productQty: HTMLInputElement){
    //get product quantity as number
    const product_qty = parseFloat(productQty.value);
    //check product availability
    if(product_qty <= product.availability || isNaN(product_qty)){
        //store table elements [td]
        const table_elements = productRow.children;
        //call add product method with ID and input value
        const value = parseInt(productQty.value);
        const opak_value = this.productMethods.hasRoundTo(productRow);
        //check if product has a quantity value
        let product_hero: Product = product;
        if(product !== null ){
          //check if there is a discount
          let discount_input = (<HTMLInputElement>productRow.querySelector('.product-discount'));
          let discount;
          if(discount_input !== null)
            discount = discount_input.value;
          let discount_val = discount == null ? '' : discount;
          if(discount_val != ''){
            //get discount prices
            let new_price_netto = this.searchTableElement(table_elements, 'product-price-netto').innerHTML.split(' ')[0];
            let new_price_brutto = this.searchTableElement(table_elements, 'product-price-brutto').innerHTML.split(' ')[0];
            //initial product with new prices
            //product.priceNetto = parseFloat(new_price_netto);
            //product.priceBrutto = parseFloat(new_price_brutto);
            product.discountInit = parseFloat(discount_val);
          }
          this.cartMethods.add(productQty, product, opak_value);
          this.cartService.addProduct(product);
          //show or hide cart button
          this.checkCartButton();
          //show status info
          this.statusInfo(`${this.translate.instant('add_to_cart_message')}: ${product.name}`, `(${productQty.value.toString()})`, 'success');
        }
        //if not then show status message
        else{
          //show status info
          this.statusInfo(`${this.translate.instant('zero_message')}`, '', 'alert');
        } 
    }
    else{
      this.statusInfo(`${this.translate.instant('availability_message')}`, '', 'alert');
    }
  }

  searchTableElement(elements: any, className: string){
    //search table elements by class name
    for(let i = 0; i < elements.length; i++){
      if(elements[i].className == className){
        return (<HTMLElement>elements[i]);
      }
    }
  }

  statusInfo(message: string, value: string, type: string) {
    this.snackBar.open(message, value, {
      duration: 2000,
      extraClasses: [type]
    });
  }

  imagePreview(element: HTMLImageElement){
    //this.helpers.imagePreview(element, product_name);
    //let allow_product_image_preview =  (this.settings.allow_product_image_preview.value == 'true');
    let show_image_preview_navigation = (this.settings.show_image_preview_navigation.value == 'true');
    let allow_to_image_preview_download = (this.settings.allow_to_image_preview_download.value == 'true');
    // this.imagePreviewExt.imagePreview(element, show_image_preview_navigation, allow_to_image_preview_download, row);
    this.imagePreviewExt.imagePreview(element, show_image_preview_navigation, allow_to_image_preview_download)
  }


  checkCartButton(): void{
    //check if any item is already in the cart, if yes then show cart button
    if(this.cartService.getCartItems().filter(Boolean).length != 0)
      this.cartButton = true;
  }

  roundTo(id, value){
      //get table product td add round actions
      const td_add_round_actions = document.querySelector('tr[data-product-row="' + id + '"] td.add-round-actions');
      //check if product has round functionality
      let table_round = td_add_round_actions.getElementsByClassName('round-table');
      if(table_round.length > 0){
        //get rounded value
        const round_val = parseFloat(table_round[0].getElementsByClassName('round-value')[0].innerHTML);
        //call the method
        this.productMethods.roundTo(id, parseInt(value), round_val, false);
        //show status info
        this.statusInfo(`${this.translate.instant('logistic_min_message')}`, `(${round_val})`, 'warning');
      }
      else
        return;
  }

  initialDiscount(product: Product, value:number): void{
    if(value <= 100){
      this.productMethods.initialDiscount(product, value);
      this.statusInfo(`${this.translate.instant('prices_updated')}`, '', 'warning');
    }
    else
      this.statusInfo(`${this.translate.instant('client_discount_value_max_message')}`, '', 'alert');
  }

  FillProductProperties(): void{
      //get cart products
      let cart: Product[] = JSON.parse(localStorage.getItem('cart'));
      let fillValues: Product[] = [];
      //check if cart is not empty
      if(cart != null){
        //if not then add all cart products to the new array
        cart.forEach(p => {
            fillValues.push(p);
        });
        //fill product input value only if product is already in the cart
        fillValues.forEach(p => {
            //get product ID
            const product_id = p.productID;
            //set product quantity added to cart
            let set_quantity_value = (<HTMLInputElement>document.querySelector('input[data-id="' + product_id + '"]')).value = p.quantity.toString();
            //set product discount percent, check if settings parameter is on
            if(p.discountInit != null && p.discountInit != 0 && !this.helpers.isNull(this.settings)){
                if(this.settings.show_client_discount.value == 'on'){
                  //let set_discount = (<HTMLInputElement>document.querySelector('tr[data-product-row="' + product_id + '"] .product-discount')).value = p.discountInit.toString(); 
                    //this.productMethods.setProductDiscountPricesHTML(p);
                }
            }
            //set product availability
            let set_product_availability = this.productMethods.checkProductAvailability(p, p.quantity);
          });
        }
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages)
        return;
    // get pager object from service
    this.pager = this.paginationService.getPager(this.allItems.length, page);

    // get current page of items
    this.products = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    this.products = this.productService.sortProducts(this.products, this.sortType);
  }

  /*
  filterTable(value: string){
    const filter_products = this.products.filter(singleItem => singleItem['name'].toLowerCase().includes(value.toLowerCase()));
    if(filter_products.length != 0){
      this.products = filter_products
    }
    else
      this.products = this.productService.getProducts();
  }
  */

  // searchTable(value: string){
  //   const hero = this.productService.getProducts();
  //   const obs = Observable.from(hero).subscribe(
  //     v => { 
  //       // const products = hero.filter(singleItem => singleItem['name'].toLowerCase().includes(value.toLowerCase()));
  //       //const products_q = hero.filter(x => hero.indexOf(x) > 0)
        
  //       let mag: Product[] = [];
  //       for(let i=0; i < hero.length; i++){
  //         if(hero[i].name.toLowerCase() == value.toLowerCase()){
  //           mag.push(hero[i]);
  //           this.products = mag;
  //         }
  //       }
  //     },
  //     err => { console.log(err)}
  //   )
  // }

  ngAfterViewInit(){
      //this.FillProductProperties();
      const cart_items = <Array<Product>>this.cartService.cartProducts.filter(Boolean);
      if(cart_items.length > 0){
        cart_items.forEach(product_cart => {
          let found_product = this.products.find(x => x.productID == product_cart.productID);
          if(!this.helpers.isNull(found_product)){
            found_product.discountInit = product_cart.discountInit;
            if(!this.helpers.isNull(found_product.discountInit)){
              found_product.priceNetto = parseFloat(this.productMethods.setDiscount(product_cart.discountInit, found_product.priceNetto));
              found_product.priceBrutto = parseFloat(this.productMethods.setDiscount(product_cart.discountInit, found_product.priceBrutto));
            }
            found_product.quantity = product_cart.quantity;
            found_product.availability = (product_cart.availability - product_cart.quantity);
          }
        });
      }
      else{
        //initial products
        //this.products = this.productService.getProducts();
      }
      this.cdr.detectChanges();
  }

}