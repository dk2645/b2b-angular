export class Message{
    constructor(public type: string, public text: string, public icon: string, public navigate: string){}
}