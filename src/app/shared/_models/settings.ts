export class Key{
    constructor(
        public name: string,
        public settings: Settings
    ){}
}

export class Settings{
    constructor(
        public type: string,
        public value: string,
        public description: string,
        public group: string
    ){}
}