import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../shared/_services/shared.service';

@Component({
    templateUrl: 'payments.component.html'
})

export class PaymentsComponent{
    constructor(private translate: TranslateService, private sharedService: SharedService) { 
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        translate.use(lang);
    }
}