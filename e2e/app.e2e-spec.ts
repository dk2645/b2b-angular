import { B2bAngularCorePage } from './app.po';

describe('b2b-angular-core App', () => {
  let page: B2bAngularCorePage;

  beforeEach(() => {
    page = new B2bAngularCorePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
